﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace Vapor
{
    class Crypto
    {
        private static RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(512);
        private static string key = "[UNKNOWN_KEY]";

        public static string encrypt(string txt, string user)
        {
            try
            {
                rsa.FromXmlString(File.ReadAllText("keys/" + user + ".pub.xml"));
                return Convert.ToBase64String(rsa.Encrypt(System.Text.Encoding.UTF8.GetBytes(txt), false));
            }
            catch
            {
                return "error";
            }
        }

        public static string decrypt(string txt)
        {
            txt = txt.Substring(2);
            rsa.FromXmlString(File.ReadAllText("keys/" + Steam3.SteamFriends.GetPersonaName().ToLower() + ".priv.xml"));
            return System.Text.Encoding.UTF8.GetString(rsa.Decrypt(Convert.FromBase64String(txt), false));
            //return txt;
        }

        public static string handshake()
        {
            return "~§" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText("keys/" + Steam3.SteamFriends.GetPersonaName().ToLower() + ".pub.xml")));
        }
    }
}
